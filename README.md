WIP Battle Of Legends

Worms Remake in Javascript

Phaser - HTML5 Game Framework

VPS - http://vps334821.ovh.net


Members:
- Damian Samsoniuk
- Filip Kamola
- Krzysztof Stelmach
- Krzysztof Majk


------------------------------------
To run webpack you have to: 

-install nodeJS : https://nodejs.org/en/
-install webpack npm package: 

    npm install webpack -g
    npm install webpack-dev-server -g

1.go to game folder
2.write in console 

    webpack-dev-server
3.and now you can go to  http://localhost:8081/webpack-dev-server/index.html

### Multi player
npm install express
npm install socket.io
node server/server.js
