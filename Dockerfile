FROM phusion/baseimage:latest

RUN \
    apt-get update && \
    apt-get install -qyy \
                    -o APT::Install-Recommends=false \
                    -o APT::Install-Suggests=false \
                                                    nginx \
                                                    nodejs \
                                                    nodejs-legacy \
                                                    npm && \
                    npm install -g typescript && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    rm -f /etc/nginx/sites-enabled/default

COPY files/ /

ENV SERVER_DIR=/var/www/server

COPY server/ ${SERVER_DIR}/

WORKDIR ${SERVER_DIR}

RUN npm install
RUN tsc || echo ok

COPY game/ /var/www/game/

EXPOSE 80 443 8000
