module.exports = {
    "extends": "eslint:recommended",
    "plugins": [
        "standard",
        "promise"
    ],
    "env": {
        "browser": true,
        "node": true
    },
    "rules":{
        "no-console": 0
    }
};