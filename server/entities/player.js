"use strict";
var Player = (function () {
    function Player(id, pos_x, pos_y) {
        this.id = id;
        this.spawn_pos_x = pos_x;
        this.spawn_pos_y = pos_y;
        this._pos_x = pos_x;
        this._pos_y = pos_y;
    }
    Object.defineProperty(Player.prototype, "pos_x", {
        get: function () {
            return this._pos_x;
        },
        set: function (x) {
            this._pos_x = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "pos_y", {
        get: function () {
            return this._pos_y;
        },
        set: function (y) {
            this._pos_y = y;
        },
        enumerable: true,
        configurable: true
    });
    Player.prototype.resetPlayer = function (id) {
        this.pos_x = this.spawn_pos_x;
        this.pos_y = this.spawn_pos_y;
    };
    return Player;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Player;
//# sourceMappingURL=player.js.map