import Player from './player';

interface Map<T> {
    [id: string]: T;
}

class Game {
    readonly id: number;
    private _players: Map<Player> = {};

    constructor(id: number) {
        this.id = id;
    }

    public addPlayer(player: Player) {
        this._players[player.id] = player
    }

    public getPlayers(): Map<Player> {
        return this._players;
    }

    public getNumPlayer(): number {
        return Object.keys(this._players).length;
    }

    public removePlayer(id: string) {
        delete this._players[id];
    }

}

export default Game;