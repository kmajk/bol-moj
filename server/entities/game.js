"use strict";
var Game = (function () {
    function Game(id) {
        this._players = {};
        this.id = id;
    }
    Game.prototype.addPlayer = function (player) {
        this._players[player.id] = player;
    };
    Game.prototype.getPlayers = function () {
        return this._players;
    };
    Game.prototype.getNumPlayer = function () {
        return Object.keys(this._players).length;
    };
    Game.prototype.removePlayer = function (id) {
        delete this._players[id];
    };
    return Game;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Game;
//# sourceMappingURL=game.js.map