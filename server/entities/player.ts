class Player {
    readonly id: number;
    readonly spawn_pos_x: number;
    readonly spawn_pos_y: number;
    private _pos_x: number;
    private _pos_y: number;

    constructor(id: number, pos_x: number, pos_y: number) {
        this.id = id;
        this.spawn_pos_x = pos_x;
        this.spawn_pos_y = pos_y;
        this._pos_x = pos_x;
        this._pos_y = pos_y;
    }

    set pos_x(x: number) {
        this._pos_x = x;
    }

    get pos_x(): number {
        return this._pos_x;
    }

    set pos_y(y: number) {
        this._pos_y = y;
    }

    get pos_y(): number {
        return this._pos_y;
    }

    public resetPlayer(id: string) {
        this.pos_x = this.spawn_pos_x;
        this.pos_y = this.spawn_pos_y;
    }
}

export default Player;