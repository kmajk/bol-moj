import * as express from 'express';
import * as http from 'http';
import * as debugModule from 'debug';
import * as websocket from 'socket.io';
import Game from './entities/game'
import Player from './entities/player'

const debug = debugModule('node-express-typescript:server');

const app: express.Express = express();
// Get port from environment and store in Express.
const port = normalizePort(process.env.PORT || '8000');
app.set('port', port);

// Create server
const server = http.createServer(app);

// Create socket listener
var io = websocket(server);

// Initilize Game
var games: Game[] = [];
const max_players: number = 100;
const max_games: number = 1;

//  Server listen on provided port (on all network interfaces).
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);


/**
 * Event listener for websocket events
 */

io.on('connection', (socket) => {
  onConnection(socket);
  socket.on("move player", onMovePlayer);
  socket.on("disconnect", onDisconnect);
  socket.on("shoot player", onShootPlayer);
});

/**
 * Event listener forWebSocket "connection" event
 */
function onConnection(socket) {
  console.log('new user connected: ', socket.id);
  // Assign game to new player
  let g_id = assignGame();

  if (g_id === null) {
    socket.to(socket.id).emit("no games");
    console.log("all games full");
    return
  }

  let g = games[g_id];
  let p = new Player(socket.id, 0, 0);

  //  Send other players to new player
  for (let player in g.getPlayers()) {
    socket.emit("new player", {id: player});
  }

  g.addPlayer(p);
  // Move player to game room
  socket.join(g.id.toString());
  // Send new game event to new player
  socket.emit("new game", {id: g.id});
  // Broadcast new player to other players in Game
  socket.broadcast.to(g.id.toString()).emit("new player", {id: p.id });
  console.log('player', p.id,'assigned to game: ', g.id);
}

/**
 * Event listener forWebSocket "move player" event
 */
function onMovePlayer(socket) {
  let player_id = this.id;
  let game_id = findPlayerGame(player_id);
  let pos_x = socket.x
  let pos_y = socket.y
  let animate = socket.animate
  let facing = socket.facing
  let shooting = socket.shooting

  if (game_id !== null) {
    this.broadcast.to(game_id.toString()).emit('move player', {
      id: player_id,
      x: pos_x,
      y: pos_y,
      animate: animate,
      facing: facing,
      shooting: shooting
    });
    console.log('player', player_id, 'has moved', pos_x, pos_y, animate, facing, shooting)
  }
}

/**
 * Event listener forWebSocket "shoot player" event
 */
function onShootPlayer(socket){
    let player_id = this.id;
    let game_id = findPlayerGame(player_id);
    let pos_x = socket.x
    let pos_y = socket.y
    let animate = socket.animate
    let facing = socket.facing
    let shooting = socket.shooting

    if (game_id !== null) {
      this.broadcast.to(game_id.toString()).emit('shoot player', {
        id: player_id,
        x: pos_x,
        y: pos_y,
        animate: animate,
        facing: facing,
        shooting: shooting
      });
      console.log('player', player_id, 'shooted', pos_x, pos_y, animate, facing, shooting)
    }
}

/**
 * Event listener forWebSocket "move player" event
 */
function onDisconnect() {
  let player_id = this.id;
  let game_id = findPlayerGame(player_id);
  console.log('found game_id: ', game_id)

  if (game_id != null) {
    games[game_id].removePlayer(player_id);
    if (games[game_id].getNumPlayer() > 0) {
      io.in(games[game_id].id.toString()).emit("remove player", {id: player_id });
      if (games[game_id].getNumPlayer() < 2) {
        io.in(games[game_id].id.toString()).emit("no opponents left");
      }
    }
    console.log('player', player_id, 'has left game: ', game_id);
  }
  console.log('player', player_id, 'has disconnect');
}

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  let bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
  let addr = server.address();
  let bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;

  debug('Listening on ' + bind);
}

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val: any): number|string|boolean {
  let port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
Assign player to game
returns
*/
function assignGame(): number|null {
  let id = null;

  // Search for game with empty slots
  if (games !== undefined) {
    for (let i: number = 0; i < games.length; i++) {
      if (games[i].getNumPlayer() < max_players) {
        id = i
        break
      }
    }
  }

  // If there are no free games check if is safe to spawn new game
  if (id === null) {
    id = games.length
    if (id >= max_games) {
      // New Games limit exceeded
      return null
    } else {
      // Spawn New Game
      let g = new Game(id);
      games.push(g);
    }
  }

  return id
}

function findPlayerGame(id: string): number|null {
  for (let i: number = 0; i < games.length; i++) {
    if (id in games[i].getPlayers()) {
      return i
    }
  }
  return null
}
