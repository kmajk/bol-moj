var BattleOfLegends = BattleOfLegends || {};

//loading the game assets
BattleOfLegends.Preload = function(){};

BattleOfLegends.Preload.prototype = {

  preload: function() {

    //GAME ASSETS
    this.load.spritesheet('player', 'assets/images/player.png', 46, 50, 28);
  	this.load.image('game_background', 'assets/images/black_mamba.png'); //FROM subtlepatterns
  	this.load.image('mario', 'assets/images/mario.png'); //FROM subtlepatterns
  	this.load.image('clouds', 'assets/images/clouds.png'); //FROM subtlepatterns
  	this.load.image('kitten', 'assets/images/kitten.png');
  	this.load.image('logo', 'assets/images/BattleOfLegendsLOGO.PNG');
  	this.load.image('start_game', 'assets/images/button.png');
  	this.load.image('ground', 'assets/images/ground.png');
    this.load.image('nextturn', 'assets/images/nextturn.png');
    this.load.image('boom', 'assets/images/laser.png');
    this.load.image('bullet', 'assets/images/bullet.png');
    this.load.audio('hot', 'assets/audio/bass.mp3');
    this.load.audio('jump', 'assets/audio/jumpu.wav');
    this.load.audio('running', 'assets/audio/run.wav');
    this.load.audio('shoot', 'assets/audio/explosion.ogg');
    this.load.audio('crouch', 'assets/audio/crouch.wav');
    this.game.load.tilemap('mario', 'assets/images/super_mario.json', null, Phaser.Tilemap.TILED_JSON);
    this.game.load.image('tiles', 'assets/images/super_mario.png');
    this.load.image('ground_1x1', 'assets/images/ground_1x1.png');
  	this.load.image('walls_1x2', 'assets/images/walls_1x2.png');
    this.load.image('tiles2', 'assets/images/tiles2.png');
    this.load.tilemap('map', 'assets/map/features_test.json', null, Phaser.Tilemap.TILED_JSON);
    
  },
  create: function() {
  	this.state.start('MainMenu');
  }
};
