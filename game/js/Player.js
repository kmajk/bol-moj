function Player (game, posx, posy) {
    this.game = game;
    this.posX = posx;
    this.posY = posy;
    this.sprite = null;
    this.cursors = null;
    this.animate = null;
    this.facing = 'left';
    this.shooting = 'no';
};

Player.prototype = {

    create: function () {
        // init sprite
        this.sprite = this.game.add.sprite(this.posX, this.posY, 'player');
        this.sprite.anchor.setTo(.5,.5);
        this.sprite.scale.setTo(.7);

        // enable game physics
        this.game.physics.arcade.enable(this.sprite);
        this.game.physics.arcade.gravity.y = 1000;
        this.game.physics.arcade.gravity.x = 0;

        // enable player physics
        this.playerSpeed = 80;
        this.sprite.body.bounce.y = 0.5;
        this.sprite.body.collideWorldBounds = true;
        this.sprite.body.setSize(32, 42, 0, 0);
        this.sprite.body.maxAngular = 500;
        this.sprite.body.angularDrag = 50;

        // add animations
        this.sprite.animations.add('stand', [0], 0, true);
        this.sprite.animations.add('walk', [24, 25, 26], 8, false, true);
        this.sprite.animations.add('jump', [6], 1, false, true);
        this.sprite.animations.add('crouch', [1], 1, false, true);
        this.sprite.animations.add('strafe', [6], 1, false, true);

        // the camera will follow the player in the world
        this.game.camera.follow(this.sprite);

        // init player animation
        this.playerStand();
      
        //// Bullets ( shooting )
        this.bullets = this.game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
      
        this.bullets.createMultiple(30, 'bullet');
        this.bullets.setAll('anchor.x', 0.5);
        this.bullets.setAll('anchor.y', 1);
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);
       // this.bullets.callAll('events.onOutOfBounds.add', 'events.onOutOfBounds', this.resetBullet, this);

        this.bulletTime = 0;
        this.current_position = true;    //true - right, false - left

    },

    addKeyboardSteering: function() {
        // input keys
        this.cursors = this.game.input.keyboard.createCursorKeys();
        this.spaceBar =  this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    },

    update: function() {

        //Collision with map
        this.game.physics.arcade.collide(this.sprite, layer);

        // Animate player 

        this.sprite.animations.play(this.animate);

        // todo: sprawdzic inne sposoby sterowania playerem - np. finate machine state
      
        //Player movement
        this.cursors.left.onDown.add(Player.prototype.playerMoveLeft, this);
        this.cursors.right.onDown.add(Player.prototype.playerMoveRight, this)
        this.cursors.up.onDown.add(Player.prototype.playerJump, this)
        this.cursors.down.onDown.add(Player.prototype.playerCrouch, this)
        
        this.cursors.left.onUp.add(Player.prototype.playerStand, this);
        this.cursors.right.onUp.add(Player.prototype.playerStand, this);
        this.cursors.up.onUp.add(Player.prototype.playerStand, this);
        this.cursors.down.onUp.add(Player.prototype.playerStand, this);
      
        //Player Shooting

        this.spaceBar.onDown.add(Player.prototype.shoot, this);  
      
      
        if (this.sprite.body.velocity.x != 0 && this.sprite.body.velocity.y != 0) {
            this.emitMove();
        }

        //Collision with map
        this.game.physics.arcade.collide(this.sprite, layer);
        this.shooting = 'no';

    },

    emitMove: function() {
        console.log("move player", {x: this.sprite.position.x, y: this.sprite.position.y, animate: this.animate, facing: this.facing, shooting: this.shooting });
        socket.emit("move player", {x: this.sprite.position.x, y: this.sprite.position.y, animate: this.animate, facing: this.facing, shooting: this.shooting });
    },
    emitShoot: function(){
        console.log("shoot player", {x: this.sprite.position.x, y: this.sprite.position.y, animate: this.animate, facing: this.facing, shooting: this.shooting });
        socket.emit("shoot player", {x: this.sprite.position.x, y: this.sprite.position.y, animate: this.animate, facing: this.facing, shooting: this.shooting });
    },

    leftStand: function () {
        if (this.animate === 'walk' && this.sprite.body.velocity.x === -150) {
            this.playerStand()
            this.current_position = false;
            this.facing = 'left';
        }
    },

    rightStand: function () {
        if (this.animate === 'walk' && this.sprite.body.velocity.x === 150) {
            this.playerStand()
            this.current_position = true;
            this.facing = 'right';
        }
    },
    playerStand: function() {

        this.sprite.body.velocity.x = 0;
        this.animate = 'stand';
        this.emitMove()
    },

    playerMoveLeft: function() {
        this.sprite.body.velocity.x = -150;
        this.sprite.scale.setTo(-.7, .7);
        this.animate = 'walk';
        this.current_position = false;
        this.facing = 'left';
    },

    playerMoveRight: function(){
        this.sprite.body.velocity.x = 150;
        this.sprite.scale.setTo(.7, .7);
        this.animate = 'walk';
        this.current_position = true;
        this.facing = 'right';
    },

    playerJump: function(){
        if (! this.sprite.body.onFloor()) {
            return;
        }
        this.sprite.body.velocity.y = -600;
    },

    playerCrouch: function(){
        this.sprite.body.velocity.x = 0;
        this.animate = 'crouch';
    },

   shoot: function() {
      if (this.game.time.now > this.bulletTime) {
        this.bullet = this.bullets.getFirstExists(false);
        if (this.bullet){
            this.bullet.angle = 90;
            if(this.current_position){
              this.bullet.reset(this.sprite.x + 7, this.sprite.y - 8);
            }else {
              this.bullet.reset(this.sprite.x - 33, this.sprite.y - 8);
            }
            
            this.bullet.body.velocity.x = this.current_position ? 800 : -800;
            this.bulletTime = this.game.time.now + 150;

            this.shooting = 'yes';
            this.emitShoot();

        }
      }
    }
}
