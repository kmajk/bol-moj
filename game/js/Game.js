var BattleOfLegends = BattleOfLegends || {};

//title screen
BattleOfLegends.Game = function(){};

// multi testing

var remotePlayers = {};

//Variables
var health = 100;


var timeCounter = 0;
var time = 5;
var current_position;
var spaceBar;
var bullet;
var bullets;
var bulletTime = 0;
var layer;
var music;
var game_timer;
var game_turn;
var next_turn;
var turn_time;

BattleOfLegends.Game.prototype = {

    setEventHandlers: function() {
        socket.on("new player", this.onNewPlayer.bind(this));
        socket.on("disconnect", this.onSocketDisconnect);
        socket.on("move player", this.onMovePlayer.bind(this));
        socket.on("shoot player", this.onShootPlayer.bind(this));
        socket.on("remove player", this.onRemovePlayer.bind(this));
    },

    create: function() {
        //set world dimensions
        this.game.world.setBounds(0, 0, 1920, 1920);
        this.game.physics.startSystem(Phaser.Physics.ARCADE);

        //Creating map section
        map = this.game.add.tilemap('map');
        this.game.stage.backgroundColor = '#123453';

        //map tile images
        map.addTilesetImage('ground_1x1');
        map.addTilesetImage('walls_1x2');
        map.addTilesetImage('tiles2');

        //SETTING COLLISION IN MAP TILE IMAGE
        map.setCollisionBetween(1, 12);
        layer = map.createLayer('Tile Layer 1');
        layer.resizeWorld();
     
        //// SOUNDS ---- currently work only in Firefox
        music = this.game.add.audio('hot');
        music.loopFull(0.2);
        music.play();

        //create player
        this.player = new Player(this.game, this.game.world.centerX, this.game.world.height -100);

        this.player.create();
        this.player.addKeyboardSteering();

        //Show section with health
        this.showHealth();

        //Time counter


        // set event handler
        this.setEventHandlers();
        //this.countTime();
      
        //NExt turn 
        this.next_turn = this.game.add.button(500, 300, 'nextturn', this.actionOnClick, this);
        this.next_turn.visible = false;
        this.next_turn.fixedToCamera = true;
      
        // Timer
        this.turn_time = 30;
        this.game.time.events.add(Phaser.Timer.SECOND * this.turn_time, this.endTurn, this);
      

    },

    update: function() {
        if (this.player !== null) {
            this.player.update();
        }

        
        for (var remote in remotePlayers) {
            if (remotePlayers[remote].created === false) {
                remotePlayers[remote].create();
                remotePlayers[remote].addKeyboardSteering();
                remotePlayers[remote].created = true;
                console.log('Remote player', remote);
            }
            remotePlayers[remote].remoteupdate();
        };

        
    },

    showHealth: function() {
        var text = 'health: ' + health + '%';
        var style = { font: "28px Arial", fill: "#000", align: "center" };
        this.showHealth = this.game.add.text(this.game.width-200,  20, text, style);
        this.showHealth.fixedToCamera = true;
    },
    render: function(){
        this.game.debug.text("Time until end of your turn: " + this.game.time.events.duration, 32, 32);

    },
    endTurn: function(){
     /*   // next turn button visibility
        this.next_turn.visible = true;

        //Disable cursors
        this.player.cursors.left._enabled = false; 
        this.player.cursors.right._enabled = false;
        this.player.cursors.up._enabled = false;
        this.player.cursors.down._enabled = false;*/
    },

    updateTime: function(){
        time++;
        //console.log("time: " + time);
        this.timeCounter.setText('Time: ' + time + ' seconds');
    },
    onSocketDisconnect: function() {
        console.log("Disconnected from socket server");
    },
    onNewPlayer: function(data) {
        console.log("New player connected: "+data.id);
        var newPlayer = new RemotePlayer(this.game, data.x, data.y);
        newPlayer.id = data.id;
        newPlayer.created = false;
        remotePlayers[newPlayer.id] = newPlayer;
        console.log("New player added: "+newPlayer);
    },
    onMovePlayer: function(data) {
        if (remotePlayers[data.id] !== undefined) {
            remotePlayers[data.id].move(data.x, data.y, data.animate, data.facing, data.shooting);
        }
    },
    onShootPlayer: function(data) {
        if (remotePlayers[data.id] !== undefined) {
            remotePlayers[data.id].shoot(data.x, data.y, data.animate, data.facing, data.shooting);
        }
    },
    onRemovePlayer: function(data) {
        if (remotePlayers[data.id] !== undefined) {
            delete remotePlayers[data.id];
        }

    },
    actionOnClick: function(){
        // next turn button visibility
        this.next_turn.visible = false;

        //Enable cursors
        this.game.time.events.add(Phaser.Timer.SECOND * this.turn_time, this.endTurn, this);
        this.player.cursors.left._enabled = true;
        this.player.cursors.right._enabled = true;
        this.player.cursors.up._enabled = true;
        this.player.cursors.down._enabled = true;
    },

};
