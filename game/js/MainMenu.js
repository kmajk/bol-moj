var BattleOfLegends = BattleOfLegends || {};

//title screen
BattleOfLegends.MainMenu = function(){};

BattleOfLegends.MainMenu.prototype = {

  create: function() {

    //Logo
    this.logo = this.game.add.sprite(this.game.world.centerX, 180, 'logo');
    this.logo.scale.setTo(.6);
    this.logo.anchor.set(.5);

    //Start button
    var start_button = this.game.add.button(this.game.width/2, 400, 'start_game', actionOnClick, this);
    start_button.anchor.set(0.5);

    function actionOnClick () {
        this.game.state.start('Game');
    // socket
    socket = io(window.location.protocol+"//"+window.location.host+":8000");

    }

  }
};