var BattleOfLegends = BattleOfLegends || {};

BattleOfLegends.Boot = function(){};

//setting game configuration and loading the assets for the loading screen
BattleOfLegends.Boot.prototype = {
  preload: function() {
  	//assets we'll use in the loading screen
    
    
    
  },
  create: function() {

      //loading screen
      this.game.stage.backgroundColor = '#fff';
    
      //have the game centered horizontally
      this.scale.pageAlignHorizontally = true;

      //physics system for movement
      this.game.physics.startSystem(Phaser.Physics.ARCADE);
      this.state.start('Preload');
  }
};
