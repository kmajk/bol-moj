var BattleOfLegends = BattleOfLegends || {};
                                    //Width //Height
BattleOfLegends.game = new Phaser.Game(1180, 600, Phaser.CANVAS, '');

//LOADING FILES
BattleOfLegends.game.state.add('Boot', BattleOfLegends.Boot);
BattleOfLegends.game.state.add('Preload', BattleOfLegends.Preload);
BattleOfLegends.game.state.add('MainMenu', BattleOfLegends.MainMenu);
BattleOfLegends.game.state.add('Game', BattleOfLegends.Game);

//STARTING GAME
BattleOfLegends.game.state.start('Boot');
