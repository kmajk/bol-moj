function RemotePlayer(game, posx, posy) {
    Player.call(this, game, posx, posy);
}

RemotePlayer.prototype = new Player();
RemotePlayer.prototype.constructor = RemotePlayer;
RemotePlayer.prototype.remoteupdate = function() {
    this.game.physics.arcade.collide(this.sprite, layer);
    this.sprite.animations.play(this.animate);
}

RemotePlayer.prototype.move = function(posx, posy, animate, facing, shooting) {
    this.sprite.x = posx
    this.sprite.y = posy
    this.sprite.facing = facing
    this.sprite.shooting = shooting
    this.sprite.animations.play(animate);

    if(facing == 'right') {
        this.sprite.body.velocity.x = 150;
        this.sprite.scale.setTo(.7, .7);
        this.animate = 'walk';
    }else{
        this.sprite.body.velocity.x = -150;
        this.sprite.scale.setTo(-.7, .7);
        this.animate = 'walk';
    }

}

RemotePlayer.prototype.shoot = function(posx, posy, animate, facing, shooting) {
    this.sprite.x = posx
    this.sprite.y = posy
    this.sprite.facing = facing
    this.sprite.shooting = shooting

      if (this.game.time.now > this.bulletTime) {
        this.bullet = this.bullets.getFirstExists(false);
        if (this.bullet){
            this.bullet.angle = 90;
            if(facing == 'right'){
              this.bullet.reset(this.sprite.x + 7, this.sprite.y - 8);
            }else {
              this.bullet.reset(this.sprite.x - 33, this.sprite.y - 8);
            }
            this.bullet.body.velocity.x = facing === 'right' ? 800 : -800;
            this.bulletTime = this.game.time.now + 150;
            this.shooting = 'yes';
        }
      }

    this.sprite.animations.play(animate);
}